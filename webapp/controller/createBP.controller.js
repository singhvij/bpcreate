sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function (Controller) {
	"use strict";

	return Controller.extend("spar.bp.create.starter.BPCreate_Starter.controller.createBP", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf spar.bp.create.starter.BPCreate_Starter.view.createBP
		 */
		onInit: function () {
			// instantiating the model of type json
			var bpData = {};
			var oModel = new sap.ui.model.json.JSONModel(bpData);
			this.getView().setModel(oModel);
			///**********Calling services**************///
			var oDataCountryCode = this._callCountryService();
			debugger;
		},
		onSubmitPress: function (oEvent) {

			// ********************** CREATE data for STORE Detail Page*******************************
			var jsonData = this.getView().getModel().getData();
			jsonData.name = this.getView().byId("idName").getValue();
			jsonData.coName = this.getView().byId("idCoName").getValue();
			jsonData.group = this.getView().byId("idGroup").getValue();
			jsonData.country = this.getView().byId("idCountry").getValue();
			jsonData.vatRegistrationNo = this.getView().byId("idVATRegistrationNo").getValue();
			jsonData.format = this.getView().byId("idFormat").getValue();
			jsonData.liquorLicenceNumber = this.getView().byId("idLiquorLicenceNumber").getValue();
			jsonData.grocerWineLicenceNumber = this.getView().byId("idGrocerWineLicenceNumber").getValue();
			jsonData.distributionLicencNumber = this.getView().byId("idDistributionLicencNumber").getValue();
			jsonData.ownerName = this.getView().byId("idOwnerName").getValue();
			jsonData.accountingClerk = this.getView().byId("idAccountingClerk").getValue();
			jsonData.commsMode = this.getView().byId("idCommsMode").getValue();
			jsonData.xmlStatement = this.getView().byId("idXMLStatement").getValue();

			jsonData.code = this.getView().byId("idCode").getValue();
			jsonData.serchTerm1 = this.getView().byId("idSerchTerm1").getValue();
			jsonData.serchTerm2 = this.getView().byId("idSerchTerm2").getValue();
			jsonData.managingDC = this.getView().byId("idManagingDC").getValue();
			jsonData.businessType = this.getView().byId("idBusinessType").getValue();
			jsonData.entityRegistrationNumber = this.getView().byId("idEntityRegistrationNumber").getValue();
			jsonData.openingReason = this.getView().byId("idOpeningReason").getValue();
			jsonData.managerName = this.getView().byId("idManagerName").getValue();
			jsonData.previousCode = this.getView().byId("idPreviousCode").getValue();
			jsonData.eftIndicator = this.getView().byId("idEFTIndicator").getValue();
			jsonData.corporateIndicator = this.getView().byId("idCorporateIndicator").getValue();
			// **********************END OF data for STORE Detail Page*******************************

			// debugger;
		},
		_callCountryService: function () {
			// $.ajax({

			// 	url: "/FS1/sap/opu/odata/sap/ZBP_MDM_LOOKUPS_SRV/CountryCodeSet?$format=json",
			// 	method: "GET",
			// 	headers: {
			// 		"Content-Type": "application/json"
			// 	},
			// 	success: function (result, xhr, data) {
			// 		sap.m.MessageToast.show("successfuly");
			// 	},
			// 	error: function (jqXHR, status, error) {
			// 		sap.m.MessageToast.show("Filed");
			// 	}
			// });
			$.get("/FS1/sap/opu/odata/sap/ZBP_MDM_LOOKUPS_SRV/CountryCodeSet?$format=json", {},
				function (data) {
					console.log("Data Loaded: " + data);
				});
		}

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf spar.bp.create.starter.BPCreate_Starter.view.createBP
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf spar.bp.create.starter.BPCreate_Starter.view.createBP
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf spar.bp.create.starter.BPCreate_Starter.view.createBP
		 */
		//	onExit: function() {
		//
		//	}

	});

});